package ru.edu;

import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class StatisticReporterImplTest {

    public static final String RESULT_TXT = "./target/reporter_result.txt";

    @Test
    public void report() {
        StatisticReporter reporter = new StatisticReporterImpl(RESULT_TXT);

        TextStatistics statistics = new TextStatistics();

        statistics.addCharsCount(50);
        statistics.addWordsCount(100);
        statistics.addCharsCountOnlyPunctuations(25);
        statistics.addCharsCountWithoutSpaces(10);
        statistics.addTopWords("aaa", 1);
        statistics.addTopWords("aab", 3);
        statistics.addTopWords("aac", 2);
        statistics.addTopWords("aad", 10);

        reporter.report(statistics);

        List<String> lines = readFile(RESULT_TXT);
        assertEquals(5, lines.size());
        assertEquals("Слов: 100", lines.get(0));
        assertEquals("Символов: 50", lines.get(1));
        assertEquals("Не пробелов: 10", lines.get(2));
        assertEquals("Символов пунктуации: 25", lines.get(3));
        assertEquals("Топ-10 популярных слов: [aaa: 1, aab: 3, aac: 2, aad: 10]", lines.get(4));

    }

    private List<String> readFile(String resultTxt) {
        List<String> lines = new ArrayList<>();
        try(
                FileReader fr = new FileReader(resultTxt);
                BufferedReader br = new BufferedReader(fr)
        ) {
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }

        }  catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}