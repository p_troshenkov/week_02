package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextAnalyzerImplTest {

    @Test
    public void analyze() {
        TextAnalyzer analyzer = new TextAnalyzerImpl();

        analyzer.analyze("12345");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(1, statistics.getWordsCount());
        assertEquals(5, statistics.getCharsCount());
        assertEquals(5, statistics.getCharsCountWithoutSpaces());
        assertEquals(0, statistics.getCharsCountOnlyPunctuations());
        assertEquals("[12345: 1]", String.valueOf(statistics.getTopWords()));


    }

    @Test
    public void analyzeMultiline() {
        TextAnalyzer analyzer = new TextAnalyzerImpl();

        analyzer.analyze("12345");
        analyzer.analyze("\n123, asd45");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(3, statistics.getWordsCount());
        assertEquals(16, statistics.getCharsCount());
        assertEquals(15, statistics.getCharsCountWithoutSpaces());
        assertEquals(1, statistics.getCharsCountOnlyPunctuations());
        assertEquals("[123: 1, 12345: 1, asd45: 1]", String.valueOf(statistics.getTopWords()));

    }

    @Test
    public void analyzeNull() {
        TextAnalyzer analyzer = new TextAnalyzerImpl();

        analyzer.analyze("");


        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(0, statistics.getWordsCount());
        assertEquals(0, statistics.getCharsCount());
        assertEquals(0, statistics.getCharsCountWithoutSpaces());
        assertEquals(0, statistics.getCharsCountOnlyPunctuations());
        assertEquals("[]", String.valueOf(statistics.getTopWords()));

    }
}