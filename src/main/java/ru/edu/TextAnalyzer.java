package ru.edu;

/**
 * Построчный анализатор текста.
 *
 * Необходимо в реализации разместить внутри контейнер TextStatistics.
 * С каждым вызовом analyze обновлять статистику.
 */
public interface TextAnalyzer {

    /**
     * Анализ строки произведения.
     *
     * @param line
     */
    void analyze(String line);

    /**
     * Получение сохраненной статистики.
     *
     * @return TextStatistic
     */
    TextStatistics getStatistic();


}
