package ru.edu;

import java.util.ArrayList;
import java.util.List;

/**
 * Необходимо реализовать методы модификации
 * и доступа к хранимым приватным переменным.
 */
public class TextStatistics {

    /**
     * Всего слов.
     */
    private long wordsCount;

    /**
     * Всего символов.
     */
    private long charsCount;

    /**
     * Всего символов без пробелов.
     */
    private long charsCountWithoutSpaces;

    /**
     * Всего знаков препинания.
     */
    private long charsCountOnlyPunctuations;

    /**
     * Список ТОП-10 популярных слов.
     */

    private List<String> topWordsList = new ArrayList<>();


    /**
     * Получение количества слов.
     *
     * @return значение
     */
    public long getWordsCount() {
        return wordsCount;
    }

    /**
     * Инкрементирование количества слов.
     *
     * @param value - значение инкремента.
     */

    public void addWordsCount(final long value) {
        wordsCount += value;
    }

    /**
     * Получение количества символов.
     *
     * @return значение
     */
    public long getCharsCount() {
        return charsCount;
    }

    /**
     * Инкрементирование количества символов.
     *
     * @param value - значение инкремента.
     */

    public void addCharsCount(final long value) {
        charsCount += value;
    }

    /**
     * Получение количества слов без пробелов.
     *
     * @return значение
     */
    public long getCharsCountWithoutSpaces() {
        return charsCountWithoutSpaces;
    }

    /**
     * Инкрементирование количества слов без пробелов.
     *
     * @param value - значение инкремента.
     */

    public void addCharsCountWithoutSpaces(final long value) {
        charsCountWithoutSpaces += value;
    }

    /**
     * Получение количества знаков препинания.
     *
     * @return значение
     */
    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    /**
     * Инкрементирование количества знаков препинания.
     *
     * @param value - значение инкремента.
     */

    public void addCharsCountOnlyPunctuations(final long value) {
        charsCountOnlyPunctuations += value;
    }

    /**
     * Задание со звездочкой.
     * Необходимо реализовать нахождение топ-10 слов.
     *
     * @return List из 10 популярных слов
     */
    public List<String> getTopWords() {
        return topWordsList;
    }

    /**
     * Добавление элементов в список из 10 популярных слов.
     * @param word - слово из текста
     * @param value - количество повторений слова
     */

    public void addTopWords(final String word, final int value) {
        topWordsList.add(word + ": " + value);
    }

    /**
     * Очищает список топ10 слов.
     */
    public void clearTopWords() {
        topWordsList.clear();
    }
    /**
     * Текстовое представление.
     *
     * @return текст
     */
    @Override
    public String toString() {
        return "TextStatistics{"
                + "wordsCount=" + wordsCount
                + ", charsCount=" + charsCount
                + ", charsCountWithoutSpaces=" + charsCountWithoutSpaces
                + ", charsCountOnlyPunctuations=" + charsCountOnlyPunctuations
                + ", topWordsList=" + topWordsList
                + '}';
    }
}
