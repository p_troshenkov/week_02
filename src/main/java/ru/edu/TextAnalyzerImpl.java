package ru.edu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TextAnalyzerImpl implements TextAnalyzer {
    /**
     * Объект, хранящий статистику анализируемого текста.
     */
    private TextStatistics statistics = new TextStatistics();
    /**
     * Словарь слов в формате <слово, количество повторений>.
     */
    private HashMap<String, Integer> wordsMap = new HashMap<>();
    /**
     * Число наиболее популярных слов.
     */
    static final int NUMBER_OF_TOP_WORDS = 10;
    /**
     * Анализ строки произведения.
     *
     * @param line
     */
    @Override
    public void analyze(final String line) {

        statistics.addCharsCount(line.length());

        String[] words = line.split(" ");
        boolean includeLetterOrDigit = false;
        for (String word : words) {

            statistics.addCharsCountWithoutSpaces(word.length());

            for (int i = 0; i < word.length(); i++) {
                if (!Character.isLetterOrDigit(word.charAt(i))) {
                    if (word.charAt(i) != '\n') {
                        statistics.addCharsCountOnlyPunctuations(1);
                    }
                } else {
                    includeLetterOrDigit = true;
                }
            }

            if (includeLetterOrDigit) {
                statistics.addWordsCount(1);
                wordsDictionary(word);
                includeLetterOrDigit = false;
            }
        }
    }

    /**
     * Получение сохраненной статистики.
     *
     * @return TextStatistic
     */
    @Override
    public TextStatistics getStatistic() {
        topWords();
        return statistics;
    }

    /**
     * Добавление слов в словарь.
     * @param word
     */
    private void wordsDictionary(final String word) {
        String wordWithoutPunctuation = "";
        for (int i = 0; i < word.length(); i++) {
            if (Character.isLetterOrDigit(word.charAt(i))) {
                wordWithoutPunctuation += word.charAt(i);
            }
        }

        if (wordsMap.containsKey(wordWithoutPunctuation)) {
            wordsMap.put(wordWithoutPunctuation,
                    wordsMap.get(wordWithoutPunctuation) + 1);
        } else {
            wordsMap.put(wordWithoutPunctuation, 1);
        }
    }

    /**
     * Поиск в словаре слов с наибольшим числом повторений.
     */
    public void topWords() {

        int maxValue;
        int iMax;
        String topWord;
        List<String> tempList = new ArrayList<>();

        statistics.clearTopWords();

        iMax = Math.min(wordsMap.size(), NUMBER_OF_TOP_WORDS);

        for (int i = 0; i < iMax; i++) {
            maxValue = 0;
            topWord = "";

            for (Map.Entry entry: wordsMap.entrySet()) {
                if (
                        ((int) entry.getValue() > maxValue)
                        && !tempList.contains(entry.getKey())
                ) {
                    topWord = String.valueOf(entry.getKey());
                    maxValue = (int) entry.getValue();
                }
            }
            if (!tempList.contains(topWord)) {
                tempList.add(topWord);
                statistics.addTopWords(topWord, maxValue);
            }
        }
    }
}
